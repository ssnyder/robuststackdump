// This file's extension implies that it's C, but it's really -*- C++ -*-.
/*
 * Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration.
 */
/**
 * @file RobustStackDump/RobustStackDump.h
 * @author scott snyder <snyder@bnl.gov>
 * @date Aug, 2023
 * @brief Set up a signal handler to generate a robust stack dump.
 */


#ifndef ROBUSTSTACKDUMP_ROBUSTSTACKDUMP_H
#define ROBUSTSTACKDUMP_ROBUSTSTACKDUMP_H


#include <vector>
#include <atomic>
#include <iostream>
#include <signal.h>


namespace RobustStackDump {


/// If true, then try to chain to the previous signal handler after the stack
/// dump.  The default is true.
extern std::atomic<bool> callOldHandler;

/// If true, then terminate the program with abort() rather than _exit(),
/// to try to allow for a core dump.  The default is false.
extern std::atomic<bool> endWithAbort;

/// Call this to change the file descriptor user for the stack dump.
/// Returns the previous file descriptor.  Passing -1 makes no changes.
int stacktraceFd (int fd = -1);


/// Install the signal handler for the signals listed in the argument.
/// Writes output to LOG.
bool installSignalHandler (const std::vector<int>& signals =
                             std::vector<int> {SIGSEGV,SIGBUS,SIGILL,SIGFPE,SIGALRM},
                           std::ostream& log = std::cerr);


/// Uninstall a previously-installed signal handler.
/// Writes output to LOG.
bool uninstallSignalHandler (std::ostream& log = std::cerr);


// Set an alternate stack to use for doing stack traces, so that we
// can continue even if our primary stack is corrupt / exhausted.
// Reserve 2MB on top of the minimum required for a signal handler.
// This sets the alternate stack for the current thread, if it hasn't
// already been done.
void setAltStack();


} // namespace RobustStackDump

#endif // not ROBUSTSTACKDUMP_ROBUSTSTACKDUMP_H
