#include "RobustStackDump/RobustStackDump.h"
#include <iostream>


int* iptr = nullptr;

int crashme()
{
  return *iptr;
}


int main()
{
  RobustStackDump::installSignalHandler();
  std::cout << crashme();
  return 0;
}
