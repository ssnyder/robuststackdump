
include(CMakeFindDependencyMacro)
include(FindPackageHandleStandardArgs)
find_package(Threads)

# - Include the targets file to create the imported targets that a client can
# link to (libraries) or execute (programs)
include("${CMAKE_CURRENT_LIST_DIR}/RobustStackDumpTargets.cmake")
get_filename_component(PACKAGE_PREFIX_DIR "${CMAKE_CURRENT_LIST_DIR}/../../../"
  ABSOLUTE)

set( RobustStackDump_INCLUDE_DIRS ${PACKAGE_PREFIX_DIR}/include ${PACKAGE_PREFIX_DIR}/include/RobustStackDump )

get_property(TEST_ROBUSTSTACKDUMP_LIBRARY TARGET RobustStackDump::RobustStackDump PROPERTY LOCATION)
find_package_handle_standard_args(RobustStackDump  DEFAULT_MSG CMAKE_CURRENT_LIST_FILE TEST_ROBUSTSTACKDUMP_LIBRARY)
