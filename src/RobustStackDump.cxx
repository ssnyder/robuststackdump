#include "RobustStackDump.h"
#include "CxxUtils/SealSignal.h"
#include "CxxUtils/SealDebug.h"
#include <vector>
#include <iostream>
#include <map>
#include <sstream>
#include <cstdint>
#include <mutex>
#include <atomic>
#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>


namespace RobustStackDump {


/// If true, then try to chain to the previous signal handler after the stack
/// dump.  The default is true.
std::atomic<bool> callOldHandler = true;

/// If true, then terminate the program with abort() rather than _exit(),
/// to try to allow for a core dump.  The default is false.
std::atomic<bool> endWithAbort = false;


// Terminate job after it this reaches the time out in Wallclock time,
// usually due to hanging during stack unwinding.
// In seconds.
constexpr int timeoutSeconds = 30*60;


constexpr const char* const horizLine = "-------------------------------------------------------------------------------------\n";


/// Map of old signal handlers.
typedef std::map<int, struct sigaction> SigHandler_t;
SigHandler_t oldSigHandler;         ///< old signal handlers
std::mutex oldSigHandlerMutex;


/// Call this to change the file descriptor user for the stack dump.
/// Returns the previous file descriptor.  Passing -1 makes no changes.
int stacktraceFd (int fd)
{
  return Athena::DebugAids::stacktraceFd (fd);
}


/// The signal handler.
void action (int sig, siginfo_t* info, void* extra)
{
  int log_fd = Athena::DebugAids::stacktraceFd();

  // Careful: don't do anything here that might allocate memory.

  // Protect against recursion.
  // We originally used a thread_local here --- but accessing
  // a thread_local can result in a call to malloc.

  const int maxcalls = 64;
  static std::atomic<int> ncalls (0);
  if (++ncalls >= maxcalls) _exit (98);

  static std::mutex tidlist_mutex;
  static size_t ntids = 0;
  static pthread_t tids[maxcalls];
  {
    pthread_t self = pthread_self();
    std::lock_guard<std::mutex> lock (tidlist_mutex);
    for (size_t i = 0; i < ntids; i++) {
      if (pthread_equal (self, tids[i])) return;
    }
    if (ntids == maxcalls) _exit (98);
    tids[ntids++] = self;
  }

  // Count the number of threads trying to dump.
  static std::atomic<int> inThreads = 0;
  ++inThreads;

  if ( sig == SIGALRM) {
    if (endWithAbort) {
      const char* msg = "Received SIGALRM. Aborting job...\n";
      (void)write (log_fd, msg, strlen(msg));
      // Restore default abort handler that should create a core file
      Athena::Signal::revert (SIGABRT);
      std::abort();
    }
    else {
      const char* msg = "Received SIGALRM. Terminating job...\n";
      (void)write (log_fd, msg, strlen(msg));
      _exit(97);   // exit without raising any further signals
    }
  }

  // Only allow one thread past at a time.
  // Try to assume as little as possible about the state of the library.
  // We don't want to hang forever here, but we also don't want
  // to call any library functions that might use signals under the hood.
  // So use nanosleep() to do the delay --- that's defined to be
  // independent of signals.
  static std::mutex threadMutex;
  const timespec one_second { 1, 0 };
  {
    unsigned int waits = 0;
    while (!threadMutex.try_lock()) {
      nanosleep (&one_second, nullptr);
      if (++waits > timeoutSeconds) _exit (97);
    }
  }

  // setup timeout
  if ( timeoutSeconds > 0 && (sig == SIGSEGV || sig == SIGBUS || sig == SIGABRT) ) {
    // This will trigger SIGALRM, which we then handle ourselves above
    alarm(timeoutSeconds);
  }

  // Do fast stack trace before anything that might touch the heap.
  // For extra paranoia, avoid iostreams/stdio and use write() directly.
  (void)write (log_fd, horizLine, strlen(horizLine));
  {
    const char* msg = "Producing (fast) stack trace...\n";
    (void)write (log_fd, msg, strlen (msg));
  }
  (void)write (log_fd, horizLine, strlen(horizLine));
  Athena::Signal::fatalDump (sig, info, extra,
                             log_fd,
                             Athena::Signal::FATAL_DUMP_SIG +
                             Athena::Signal::FATAL_DUMP_CONTEXT +
                             Athena::Signal::FATAL_DUMP_STACK);
  (void)write (1, "\n", 1);

  std::cout.flush();
  std::cerr.flush();
    

  if (callOldHandler) {
    // Call previous signal handler
    // Need to distinguish between the two different types
    struct sigaction oact;
    {
      std::scoped_lock lock (oldSigHandlerMutex);
      auto it = oldSigHandler.find(sig);
      if (it != oldSigHandler.end())
        oact = it->second;
      else {
        oact.sa_handler = SIG_DFL;
        oact.sa_flags = 0;
      }
    }
    {
      const char* msg = "Invoking previous signal handler (can be slow, check gdb process)...\n";
      (void)write (log_fd, msg, strlen(msg));
    }
    (void)write (log_fd, horizLine, strlen(horizLine));
    if ( oact.sa_flags & SA_SIGINFO ) {
      oact.sa_sigaction(sig, info, extra);
    }
    else if (oact.sa_handler != SIG_DFL && oact.sa_handler != SIG_IGN ) {
      oact.sa_handler(sig);
    }
    else {
      const char* msg = "Could not invoke previous signal handler\n";
      (void)write (log_fd, msg, strlen(msg));
    }
  }

  // This thread is done dumping.
  threadMutex.unlock();
  --inThreads;

  if (sig == SIGSEGV || sig == SIGBUS || sig == SIGABRT) {
    // Don't terminate the program while there are other threads
    // trying to dump (but don't wait forever either).
    unsigned int waits = 0;
    while (inThreads > 0 && waits < timeoutSeconds) {
      nanosleep (&one_second, nullptr);
    }

    if (endWithAbort) {
      const char* msg = "Aborting job...\n";
      (void)write (log_fd, msg, strlen(msg));
      // Restore default abort handler that should create a core file
      Athena::Signal::revert (SIGABRT);
      std::abort();
    }

    // Exit now on a fatal signal; otherwise, we can hang.
    _exit (99);
  }
}


/// Install the signal handler for the signals listed in the argument.
/// Writes output to LOG.
bool installSignalHandler (const std::vector<int>& signals,
                           std::ostream& log)
{
  std::scoped_lock lock (oldSigHandlerMutex);
  std::ostringstream oss;

  for (int sig : signals) {
#ifndef __APPLE__
    if (sig<1 || sig>SIGRTMAX) {
      log << "WARNING: Invalid signal number " << sig << ". Ignoring." << std::endl;
      continue;
    }
#endif
    oss << sig << "(" << strsignal(sig) << ") ";

    // Set up an alternate stack for this thread.
    setAltStack();
    
    // Install new signal handler and backup old one
    struct sigaction sigact;
    memset (&sigact, 0, sizeof(sigact));
    sigact.sa_sigaction = RobustStackDump::action;
    sigemptyset(&sigact.sa_mask);
    sigact.sa_flags = SA_SIGINFO + SA_ONSTACK;
    int ret = sigaction(sig, &sigact, &oldSigHandler[sig]);
    if ( ret!=0 ) {
      log << "Error on installing handler for signal " << sig
          << ": " << strerror(errno) << std::endl;
      return false;
    }
  }
  log << "Handling signals: " << oss.str() << std::endl;
  return true;
}

/// Uninstall a previously-installed signal handler.
/// Writes output to LOG.
bool uninstallSignalHandler (std::ostream& log)
{
  bool status = true;
  std::scoped_lock lock (oldSigHandlerMutex);
  for (const auto& kv : oldSigHandler) {
    int ret = sigaction(kv.first, &(kv.second), nullptr);
    if ( ret!=0 ) {
      status = false;
      log << "Error on uninstalling handler for signal " << kv.first
          << ": " << strerror(errno) << std::endl;
    }
  }
  return status;
}


// Set an alternate stack to use for doing stack traces, so that we
// can continue even if our primary stack is corrupt / exhausted.
// Reserve 2MB on top of the minimum required for a signal handler.
// This sets the alternate stack for the current thread, if it hasn't
// already been done.
void setAltStack()
{
  thread_local static std::vector<uint8_t> s_stack;
  std::vector<uint8_t>& stack = s_stack;
  if (stack.empty()) {
    stack.resize (std::max (SIGSTKSZ, MINSIGSTKSZ) + 2*1024*1024);
    stack_t ss;
    ss.ss_sp = stack.data();
    ss.ss_flags = 0;
    ss.ss_size = stack.size();
    sigaltstack (&ss, nullptr);
  }
}


} // namespace RobustStackDump
