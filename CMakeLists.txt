cmake_minimum_required( VERSION 3.10 )
project( RobustStackDump )

# Make sure optimization is on by default.
if( NOT CMAKE_BUILD_TYPE )
  set( CMAKE_BUILD_TYPE RelWithDebInfo )
endif()

# Minimum required.  Should also work with C++20 / 23.
set( CMAKE_CXX_STANDARD 17 )

add_compile_options( -Wall -Wextra )

include( GNUInstallDirs )
find_package( Threads )


############################################################################3
# Build the library.
#

include_directories( include )
add_library( RobustStackDump
             SHARED
             src/RobustStackDump.cxx
             src/UnwindBacktrace.cxx
             src/arm_helpers.cxx
             src/SealSharedLib.cxx
             src/SealDebug.cxx
             src/SealSignal.cxx )
target_link_libraries( RobustStackDump PUBLIC Threads::Threads ${CMAKE_DL_LIBS} )



############################################################################3
# Install the library, headers, and cmake files.
#

file( GLOB cxxutils_headers RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}"
      include/CxxUtils/*.h )
file( GLOB wrapper_headers RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}"
        include/*.h )

install( TARGETS RobustStackDump
  EXPORT RobustStackDumpTargets
)
install( FILES ${cxxutils_headers} DESTINATION include/RobustStackDump/CxxUtils )
install( FILES ${wrapper_headers} DESTINATION include/RobustStackDump )

install(EXPORT ${PROJECT_NAME}Targets
  NAMESPACE ${PROJECT_NAME}::
  FILE "${PROJECT_NAME}Targets.cmake"
  DESTINATION "${CMAKE_INSTALL_LIBDIR}/cmake/${PROJECT_NAME}/"
  )
install(FILES cmake/${PROJECT_NAME}Config.cmake 
        DESTINATION "${CMAKE_INSTALL_LIBDIR}/cmake/${PROJECT_NAME}/" )


############################################################################3
# Tests.
#

set( CMAKE_CTEST_ARGUMENTS --output-on-failure )
enable_testing()
set( test_bin_dir ${CMAKE_CURRENT_BINARY_DIR}/testbins )
set( test_run_dir ${CMAKE_CURRENT_BINARY_DIR}/run-tests )
set( run_test_script ${CMAKE_CURRENT_SOURCE_DIR}/test/run-test.sh )
file( MAKE_DIRECTORY ${test_run_dir} )

set( test_libs RobustStackDump )
set( test_definitions )
set( test_includes )

function( _add_test name )
  cmake_parse_arguments( ARG "NOREF" "" "" ${ARGN} )
  set( test_name ${name}_test )
  set( test_target ${test_name}.exe )
  set( test_exe ${test_bin_dir}/${test_target} )
  set( test_source ${CMAKE_CURRENT_SOURCE_DIR}/test/${test_name}.cxx )
  if( ARG_NOREF )
    set( test_ref )
  else()
    set( test_ref ${CMAKE_CURRENT_SOURCE_DIR}/test/${test_name}.ref )
  endif()
  add_executable( ${test_target} ${test_source} )
  target_link_libraries( ${test_target} ${test_libs} )
  target_compile_definitions( ${test_target} PRIVATE ${test_definitions} )
  set_target_properties( ${test_target} PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${test_bin_dir} )
  if( test_includes )
    target_include_directories( ${test_target} PRIVATE ${test_includes} )
  endif()
  add_test( NAME ${test_name}
    COMMAND ${run_test_script} ${test_exe} "${test_ref}"
    WORKING_DIRECTORY ${test_run_dir} )
endfunction( _add_test )


_add_test( stacktrace )
