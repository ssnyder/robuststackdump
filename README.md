This repository contains code for the robust stack dump described 
in the CHEP 23 proceedings _Multithreading ATLAS offline software: a retrospective_.
Most of the code in this repository is simple copies from the ATLAS Athena
repository <https://gitlab.cern.ch/atlas/athena>, mostly from the
Control/CxxUtils directory 
(<https://gitlab.cern.ch/atlas/athena/tree/main/Control/CxxUtils>).
See there for the history of the code.  The code in RobustStackDump.cxx
is derived from the Athena [CoreDumpSvc](https://gitlab.cern.ch/atlas/athena/tree/main/Control/AthenaServices/src/CoreDumpSvc.cxx)
class, but edited to remove othe Athena dependencies.

A CMake file is provided to build and install the library and run the tests.
The build was tested on EL8 (x86_64) and EL9 (x86_64/aarch64).  Other platforms
may also work as long as the compiler supports at least c++17.

The example directory gives a very simple example of a program building
against the library.
