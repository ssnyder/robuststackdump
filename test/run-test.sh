#!/bin/sh

test=$1
ref=$2

test_root=`basename $test .exe`
test_log=${test_root}.log

$test > $test_log
teststatus=$?
if [ $teststatus != 0 ]; then
    echo "ERROR: Test returns status $teststatus"
    cat ${test_root}.log
    exit $teststatus
fi
    
if [ "$ref" != "" ]; then
    grep -Ev '0x[0-9a-f]{4,}|DebugAids::stacktrace|CxxUtils::backtraceByUnwind|DWARF error' < $test_log > ${test_log}-todiff
    diff -u $ref ${test_log}-todiff > ${test_root}.diffs
    diffstatus=$?
    if [ $diffstatus != 0 ] ; then
        echo "ERROR: Test result differs"
        cat ${test_root}.diffs
        exit 1
    fi
fi

